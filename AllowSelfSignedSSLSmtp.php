<?php

/**
 * AllowSelfSignedSSLSmtp : Allow self signed ssl signature for SMTP server
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2021-2024 Denis Chenu <http://www.sondages.pro>
 * @license GPL
 * @version 0.3.1
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */
class AllowSelfSignedSSLSmtp extends PluginBase
{
    protected $storage = 'DbStorage';

    protected static $description = 'Allow self signed ssl signature for SMTP server';
    protected static $name = 'AllowSelfSignedSSLSmtp';


    public function init()
    {
        if (intval(App()->getConfig('versionnumber')) >= 4) {
            $this->subscribe('beforeTokenEmail', 'beforeEmail');
            $this->subscribe('beforeSurveyEmail', 'beforeEmail');
            $this->subscribe('beforeEmail');
        } else {
            $this->subscribe('beforeTokenEmailExtended', 'beforeEmail');
        }
    }

    /**
     * @link https://manual.limesurvey.org/beforeEmail
     */
    public function beforeEmail()
    {
        $Mailer = $this->getEvent()->get('mailer');
        if (empty($Mailer)) {
            $Mailer = $this->getEvent()->get('PhpMailer');
        }
        if (empty($Mailer)) {
            $this->log("No mailer found in beforeEmail event (checked mailer and PhpMailer).", 'error');
        }
        $Mailer->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
    }
}
