# AllowSelfSignedSSLSmtp

Just allow to use self signed SMTP server easily with LimeSurey.

## Home page & Copyright
- HomePage <http://extensions.sondages.pro/>
- Copyright © 2021-2023 Denis Chenu <http://sondages.pro>
- Licence : GNU General Public License <https://www.gnu.org/licenses/gpl-3.0.html>